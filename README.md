# Food Ordering

## Description

This is a sample app provided to show a food ordering application. This includes a home view where people can choose products and a cart view to review user's orders.

![Food Ordering](https://gitlab.com/dsiancas1/swiftui/foodordering/-/raw/main/food-ordering.gif)

The architectured used was MVVM with the following technologies:

* Declarative UI using SwiftUI.
* Core locations for location services.
* Anonymous authentication and serverless backend using Firebase.


