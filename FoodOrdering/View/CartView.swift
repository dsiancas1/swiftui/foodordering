//
//  CartView.swift
//  FoodOrdering
//
//  Created by Daniel Siancas on 3/02/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct CartView: View {
    @ObservedObject var viewModel: HomeViewModel
    @Environment(\.presentationMode) var present
    var body: some View {
        VStack {
            HStack(spacing: 20) {
                Button(action: { present.wrappedValue.dismiss() }) {
                    Image(systemName: "chevron.left")
                        .font(.system(size: 26, weight: .heavy))
                        .foregroundColor(Color("pink"))
                }

                Text("My cart")
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundColor(.black)

                Spacer()
            }
            .padding()

            ScrollView(.vertical, showsIndicators: false) {
                LazyVStack(spacing: 0) {
                    ForEach(viewModel.cartItems) { cart in
                        HStack(spacing: 15) {
                            WebImage(url: URL(string: cart.item.item_image))
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .frame(width: 130, height: 130)
                                .cornerRadius(15)

                            VStack(alignment: .leading, spacing: 10) {
                                Text(cart.item.item_name)
                                    .fontWeight(.semibold)
                                    .foregroundColor(.black)

                                Text(cart.item.item_details)
                                    .fontWeight(.semibold)
                                    .foregroundColor(.gray)
                                    .lineLimit(2)

                                HStack(spacing: 15) {
                                    Text(viewModel.getPrice(value: Float(truncating: cart.item.item_cost)))
                                        .font(.title2)
                                        .fontWeight(.heavy)
                                        .foregroundColor(.black)

                                    Spacer(minLength: 0)

                                    Button(action: {
                                        if cart.quantity > 1 {
                                            viewModel.cartItems[viewModel.getIndex(item: cart.item,isCartIndex: true)].quantity -= 1

                                        }
                                    }) {
                                        Image(systemName: "minus")
                                            .font(.system(size: 16, weight: .heavy))
                                            .foregroundColor(.black)
                                    }

                                    Text("\(cart.quantity)")
                                        .fontWeight(.heavy)
                                        .foregroundColor(.black)
                                        .padding(.vertical,5)
                                        .padding(.horizontal,10)
                                        .background(Color.black.opacity(0.06))

                                    Button(action: {
                                        viewModel.cartItems[viewModel.getIndex(item: cart.item,isCartIndex: true)].quantity += 1
                                    }) {
                                        Image(systemName: "plus")
                                            .font(.system(size: 16, weight: .heavy))
                                            .foregroundColor(.black)
                                    }
                                }
                            }
                        }
                        .padding()
                        .contentShape(RoundedRectangle(cornerRadius: 15))
                        .contextMenu {
                            Button(action: {
                                let index = viewModel.getIndex(item: cart.item, isCartIndex: true)
                                let itemIndex = viewModel.getIndex(item: cart.item, isCartIndex: false)

                                let filterIndex = viewModel.filtered.firstIndex { (item1) -> Bool in
                                    return cart.item.id == item1.id
                                } ?? 0

                                viewModel.items[itemIndex].isAdded = false
                                viewModel.filtered[filterIndex].isAdded = false

                                viewModel.cartItems.remove(at: index)
                            }) {
                                Text("Remove")
                            }
                        }
                    }
                }
            }

            VStack {
                HStack {
                    Text("Total")
                        .fontWeight(.heavy)
                        .foregroundColor(.gray)

                    Spacer()

                    Text(viewModel.calculateTotalPrice())
                        .font(.title)
                        .fontWeight(.heavy)
                        .foregroundColor(.black)
                }
                .padding([.top, .horizontal])

                Button(action: viewModel.updateOrder) {
                    Text(viewModel.ordered ? "Cancel Order" : "Check out")
                        .font(.title2)
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        .padding(.vertical)
                        .frame(width: UIScreen.main.bounds.width - 30)
                        .background(
                            Color("pink")
                        )
                        .cornerRadius(15)
                }
            }
            .background(Color.white)

        }
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}
