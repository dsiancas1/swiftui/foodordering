//
//  Home.swift
//  FoodOrdering
//
//  Created by Daniel Siancas on 2/02/21.
//

import SwiftUI

struct Home: View {
    @StateObject var viewModel = HomeViewModel()
    
    var body: some View {
        ZStack {
            VStack(spacing: 10) {
                HStack(spacing: 15) {
                    
                    Button(action: {
                        withAnimation(.easeIn) {
                            viewModel.showMenu.toggle()
                        }
                    }, label: {
                        Image(systemName: "line.horizontal.3")
                            .font(.title)
                            .foregroundColor(Color("pink"))
                    })
                    
                    Text(viewModel.userLocation == nil ? "Locating..." : "Deliver To")
                        .foregroundColor(.black)
                    
                    Text(viewModel.userAddress)
                        .font(.caption)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("pink"))
                    
                    Spacer(minLength: 0)
                }
                .padding([.horizontal, .top])
                
                Divider()
                
                HStack(spacing: 15) {
                    Image(systemName: "magnifyingglass")
                        .font(.title2)
                        .foregroundColor(.gray)
                    
                    TextField("Search", text: $viewModel.search)
                }
                .padding(.horizontal)
                .padding(.top, 10)
                
                Divider()
                
                if viewModel.items.isEmpty {
                    Spacer()
                    ProgressView()
                    Spacer()
                } else {
                    ScrollView(.vertical, showsIndicators: false, content: {
                        VStack(spacing: 25) {
                            ForEach(viewModel.filtered) { item in
                                ZStack(alignment: Alignment(horizontal: .center, vertical: .top), content: {
                                    ItemView(item: item)
                                    
                                    HStack {
                                        Text("FREE DELIVERY")
                                            .foregroundColor(.white)
                                            .padding(.vertical, 10)
                                            .padding(.horizontal)
                                            .background(Color("pink"))
                                        
                                        Spacer(minLength: 0)
                                        
                                        Button(action: {
                                            viewModel.addToCart(item: item)
                                        }, label: {
                                            Image(systemName: item.isAdded ? "checkmark" : "plus")
                                                .foregroundColor(.white)
                                                .padding(10)
                                                .background(item.isAdded ? Color.green : Color("pink"))
                                                .clipShape(Circle())
                                        })
                                    }
                                    .padding(.trailing, 10)
                                    .padding(.top, 10)
                                })
                                .frame(width: UIScreen.main.bounds.width - 30)
                            }
                        }
                        .padding(.top, 10)
                    })
                }
            }
            
            HStack {
                Menu(viewModel: viewModel)
                    .offset(x: viewModel.showMenu ? 0 : -UIScreen.main.bounds.width / 1.6)
                
                Spacer(minLength: 0)
            }
            .background(
                Color.black.opacity(viewModel.showMenu ? 0.3 : 0).ignoresSafeArea()
                    .onTapGesture(perform: {
                        withAnimation(.easeIn) {
                            viewModel.showMenu.toggle()
                        }
                    })
            )
            
            if viewModel.noLocation {
                Text("Please enable location access in settings to further move on!!")
                    .foregroundColor(Color.black)
                    .frame(width: UIScreen.main.bounds.width - 100, height: 120)
                    .background(Color.white)
                    .cornerRadius(10)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color.black.opacity(0.3).ignoresSafeArea())
            }
        }
        .onAppear(perform: {
            viewModel.locationManager.delegate = viewModel
        })
        .onChange(of: viewModel.search, perform: { value in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if value == viewModel.search && !viewModel.search.isEmpty {
                    viewModel.filterData()
                }
            }
            
            if viewModel.search.isEmpty {
                withAnimation(.linear) { viewModel.filtered = viewModel.items }
            }
        })
    }
}
