//
//  Menu.swift
//  FoodOrdering
//
//  Created by Daniel Siancas on 3/02/21.
//

import SwiftUI

struct Menu: View {
    @ObservedObject var viewModel = HomeViewModel()
    var body: some View {
        VStack {
            NavigationLink(destination: CartView(viewModel: viewModel)) {
                HStack(spacing: 15) {
                    Image(systemName: "cart")
                        .font(.title)
                        .foregroundColor(Color("pink"))
                        
                    Text("Cart")
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                        
                    Spacer(minLength: 0)
                }
                .padding()
            }
            
            Spacer()
            
            HStack {
                Spacer()
                
                Text("Version 0.1")
                    .fontWeight(.bold)
                    .foregroundColor(Color("pink"))
            }
            .padding(10)
        }
        .padding([.top, .trailing])
        .frame(width: UIScreen.main.bounds.width / 1.6)
        .background(Color.white.ignoresSafeArea())
    }
}
