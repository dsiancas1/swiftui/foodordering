//
//  Item.swift
//  FoodOrdering
//
//  Created by Daniel Siancas on 3/02/21.
//

import Foundation

struct Item: Identifiable {
    var id: String
    var item_name: String
    var item_cost: NSNumber
    var item_details: String
    var item_image: String
    var item_ratings: String
    
    var isAdded: Bool = false
}
