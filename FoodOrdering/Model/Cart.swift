//
//  Cart.swift
//  FoodOrdering
//
//  Created by Daniel Siancas on 3/02/21.
//

import Foundation

struct Cart: Identifiable {
    var id = UUID().uuidString
    var item: Item
    var quantity: Int
}
